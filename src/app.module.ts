import {Module} from '@nestjs/common';
import {EasyconfigModule, EasyconfigService} from "nestjs-easyconfig";
import {MongooseModule} from "@nestjs/mongoose";
import {FrequentQuestionsModule} from "./common/modules/frequent-questions.module";
import {NestCrawlerModule} from "nest-crawler/dist";

@Module({
    imports: [
        EasyconfigModule.register({path: `./src/config/.env.${process.env.NODE_ENV || "dev"}`}),
        MongooseModule.forRootAsync({
            imports: [EasyconfigModule],
            useFactory: async (cfg: EasyconfigService) => ({
                uri: cfg.get('DB_URL'),
            }),
            inject: [EasyconfigService],
        }),
        NestCrawlerModule,
        FrequentQuestionsModule
    ]
})
export class AppModule {
}
