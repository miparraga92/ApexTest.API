import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import * as fs from "fs";

async function bootstrap() {
  const httpsOptions = {
    key: fs.readFileSync('./secrets/apex-test-key.pem'),
    cert: fs.readFileSync('./secrets/apex-test.pem'),
  };
  const app = await NestFactory.create(AppModule, {httpsOptions} );
  app.setGlobalPrefix('api');
  app.enableCors();
  const options = new DocumentBuilder()
      .setTitle('Apex Test API')
      .setDescription('Apex Test API')
      .setVersion('1.0')
      .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);
}
bootstrap();
