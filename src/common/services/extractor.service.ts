import {Injectable} from '@nestjs/common';
import {NestCrawlerService} from "nest-crawler/dist";
import {EasyconfigService} from "nestjs-easyconfig";
import * as moment from "moment";
import {QuestionListDto} from "../dtos/question-list.dto";

@Injectable()
export class ExtractorService {
    constructor(
        private readonly crawler: NestCrawlerService,
        private readonly easyconfigService: EasyconfigService
    ) {
    }

    public async extract(): Promise<QuestionListDto> {
        const result: QuestionListDto = await this.crawler.fetch({
            target: this.easyconfigService.get('EXTRACT_URL'),
            fetch: {
                result: {
                    listItem: 'app-faq-question',
                    data: {
                        index: '.faq-title_question',
                        value: {
                            selector: '.faq-text > div > div',
                            how: 'html'
                        }
                    }
                }
            }
        });
        result.updated = moment().format('YYYY-MM-DD HH:mm');
        return result;
    }
}
