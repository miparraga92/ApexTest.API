import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {QuestionModel} from "./question.model";
import {Document} from "mongoose";

@Schema()
export class QuestionListModel extends Document {
    @Prop()
    result: QuestionModel[];
    @Prop()
    updated: string;
}

export const QuestionListSchema = SchemaFactory.createForClass(QuestionListModel);
