import {Model, Document} from "mongoose";

export class BaseRepository<TModel extends Document, TDto> {

    constructor(private model: Model<TModel>) {
    }

    async insert(entity: TDto): Promise<TModel> {
        const createdModel = new this.model(entity);
        return createdModel.save();
    }

    async all(): Promise<TModel[]> {
        return this.model.find().exec();
    }

    async find(id: any): Promise<TModel> {
        return this.model.findById(id).exec();
    }

    async update(entity: TModel): Promise<TModel> {
        return this.model.replaceOne({_id: entity._id }, entity).exec();
    }

    async delete(id: any): Promise<void> {
        this.model.deleteOne({_id: id}).exec();
    }

    async deleteAll(): Promise<void> {
        this.model.deleteMany({}).exec();
    }
}
