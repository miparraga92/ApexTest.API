import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {QuestionListModel} from "../models/question-list.model";
import {QuestionListDto} from "../dtos/question-list.dto";
import {BaseRepository} from "./base.repository";

@Injectable()
export class QuestionListRepository extends BaseRepository<QuestionListModel, QuestionListDto>{
    constructor(@InjectModel('QuestionList') private entityModel: Model<QuestionListModel>) {
        super(entityModel);
    }
}
