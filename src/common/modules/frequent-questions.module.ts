import {MongooseModule} from "@nestjs/mongoose";
import {FrequentQuestionsController} from "../controllers/frequent-questions.controller";
import {Module} from "@nestjs/common";
import {NestCrawlerModule} from "nest-crawler/dist";
import {ExtractorService} from "../services/extractor.service";
import {QuestionListSchema} from "../models/question-list.model";
import {QuestionListRepository} from "../repositories/question-list.repository";
import {QuestionListService} from "../services/question-list.service";
import {JsonService} from "../services/json.service";

@Module({
    imports: [
        MongooseModule.forFeature([
        {name: 'QuestionList', schema: QuestionListSchema}
        ]),
        NestCrawlerModule
    ],
    controllers: [FrequentQuestionsController],
    providers: [ExtractorService, QuestionListRepository, QuestionListService, JsonService]
})
export class FrequentQuestionsModule {}
